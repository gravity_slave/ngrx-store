import { NgrxStorePage } from './app.po';

describe('ngrx-store App', () => {
  let page: NgrxStorePage;

  beforeEach(() => {
    page = new NgrxStorePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
