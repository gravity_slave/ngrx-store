export interface Thread {
  id: number;
  messageIds: Array<number>;
  participants:{[key: number]: number}
}
